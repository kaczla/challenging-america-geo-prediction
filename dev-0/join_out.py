with open('out1.tsv') as fo1, open('out2.tsv') as fo2, open('out.tsv','w') as fo:
    for lo1, lo2 in zip(fo1, fo2):
        fo.write(lo1.rstrip('\n') + '\t' + lo2.rstrip('\n') + '\n')
